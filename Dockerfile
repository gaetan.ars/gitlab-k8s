FROM zenman94/pandoc:2.4 AS builder

WORKDIR /app

COPY . .

RUN pandoc -s -f markdown -t revealjs -o index.html slides.md -V revealjs-url=https://revealjs.com -V transition=concave -V theme=blood --include-in-header=css/perso.css

FROM nginx:stable-alpine

COPY --from=builder /app/index.html /usr/share/nginx/html/
COPY --from=builder /app/css /usr/share/nginx/html/css
COPY --from=builder /app/img /usr/share/nginx/html/img