---
title: De GitLab à Kubernetes
subtitle: Meetup CNCF Bordeaux \#2
author: Gaëtan Ars
date: 11 Décembre 2018
---

### A propos de moi

Ingénieur expert leader chez Orange Applications for Business

Equipe outillage de production : étude, construction et déploiement des outils du RUN

### A propos de vous
::: incremental

- Les familiers de GitLab ?

- Les coutumiers de GitLab-CI ?

- Les as de Kubernetes ?

:::

### Contenu

* GitLab

* GitLab-CI

* Kubernetes

* Marions-les !

## GitLab

### Présentation

A l'origine : logiciel libre de forge basé sur Git

GtiLab B.V : Entreprise devenue GitLab Inc en 2015

GitLab CE : Community Edition, libre (licence MIT)

GitLab EE : Entreprise Edition, propriétaire

GitLab.com : service en ligne, basé sur GitLab EE

### Qu'est-ce ?

Un outil de contrôle des sources basé sur Git

Gére les dépôts, les utilisateurs et les droits

Propose des outils de travail collaboratif et d'automatisation

### Workflow

Définit la façon de travailler en équipe

![](img/git-model.png)

### Vous avez dit DevOps ?

![](img/gitlab-devops.png)

> "From project planning and source code management to CI/CD and monitoring, GitLab is a single application for the entire DevOps lifecycle."

::: notes

Gestion de projet, gestion du code, intégration, déploiement continue et monitoring, une application pour tout le cycle DevOps

:::

## GiLab-CI(/CD)

### Présentation

GitLab CI depuis 2012 et CI/CD depuis 2016

Intégré à GitLab, disponible dans GitLab CE, il est multi-plateforme et multi-langage

GitLab CI permet de faire de l'intégration continue, de la livraison ou du déploiement continu

![](img/cicd_pipeline_infograph.png){width=80% height=80%}

### Comment ça marche ?

On définit l'ensemble des éléments dans le manifeste `.gitlab-ci.yml`{.shell-session} à la racine du projet, il fait donc parti du code, il est collaboratif et versionné

A chaque commit ou push, le pipeline sera déclenché et les actions seront jouées à la demande par un agent : GitLab Runner

C'est un binaire GO (multi-plateforme) qui communique via l'API GitLab

### Pipeline

Un *job* représente une tâche. Un *stage* est composé d'un ou plusieurs jobs. L'ensemble des *stages* définit le *pipeline*.

Dans un *pipeline*, les *stages* sont exécutés séquentiellement et les jobs d'un même *stage* sont exécutés parrallèlement

![](img/pipelines.png)

### .gitlab-ci.yml
```yaml
stages:
  - build
  - test
  - staging
  - production

build:
  stage: build
  script: make build

test1:
  stage: test
  script: make test2

test2:
  stage: test
  script: make test1

auto-deploy-management:
  stage: staging
  script: make deploy staging

deploy to production: 
  stage: production
  script: make deploy production
```

### Conditions

L'exécution des jobs peut-être conditionnée : uniquement sur une branche, sur un tag, sur toutes les branches sauf ....

Un pipeline peut, selon les conditions avoir un stage qui ne contient pas de job à exécuter

La contruction d'un pipeline est étroitement liée au workflow adopté pour le projet

### Variables

Lorsque le Runner prépare l'exécution d'un job, il commence par définir un ensemble de variables d'environnement et de variables utilisateur

On peut définir des variables dans le fichier `.gitlab-ci.yml`, mais aussi des variables de groupe ou de projet dans GitLab

On peut les protéger pour ne les exposer que sur des branches protégées ou des tags

### Environnements

Permet de définir un environnement spécifique au déploiement et les conditions qui y sont associées

Les environnements peuvent être dynamiques (variabilisés dans le CI)

GitLab intègre **Prometheus** pour monitorer les environnements

## Kubernetes

### Définition

> "Kubernetes is an open-source system for automating deployment, scaling, and management of containerized applications."

Les *nodes* sont les machines (physiques ou virtuelles) où les conteneurs sont déployés

L'unité de base de Kubernetes est le *pod* dans lequel un ou plusieurs conteneurs sont colocalisés sur le même *node* et peuvent en partager les ressources

### Architecture

![](img/Kubernetes.png)

### Quelques concepts

Kubernetes peut partager sur un même serveur plusieurs clusters virtuels, on les appelle des *namespaces*

Les accès aux différentes ressources du cluster peuvent être gérés par des *Role-based access control (RBAC)*

### Ingress

On peut gérer les accès externes au cluster via des *Ingress*, ils permettent d'implémenter du load-balancing, des terminaisons SSL et des hôtes virtuels

Les *ingress* vont créer des points d'accès HTTP/HTTPS externes et les router vers des *services* de Kubernetes

Pour utiliser des *ingress*, on doit d'abord déployer un *ingress controller* (HAProxy, NGINX, Traefik, Istio ...)

### Déploiements

L'opérateur pilote le cluster via l'API en utilisant le CLI *kubectl*, il peut agir sur l'ensemble des objets du cluster en fonction des droits qu'il possède

Il peut déployer des applications et des services en écrivant des manifestes
 
Il existe aussi un gestionnaire de paquets pour Kubernetes, Helm. Le client (Helm) a besoin d'un serveur (Tiller) déployé sur le cluster

## Marions-les ! {data-background="img/mariage.gif"}

### Oui mais pour quoi faire ?
::: incremental

- Utiliser Kubernetes pour exécuter un ou des Runners
- Déployer nos applications dans Kubernetes
- Créer des environnements temporaires (review App)
- Voir les logs des conteneurs
- Remonter le monitoring
- Accéder au terminal
- Utiliser Auto DevOps (CI/CD pré-défini)

:::

### Les mariés

![](img/gitlab-gke.svg)

### Préparation du cluster

Créer un compte de service
```yaml
$ kubectl create -f - <<EOF
  apiVersion: v1
  kind: ServiceAccount
  metadata:
    name: gitlab
    namespace: default
EOF
```

### Préparation du cluster

Récupérer le token et le CA
```shell-session
$ kubectl get secret
NAME                  TYPE                                  DATA      AGE
default-token-pshlv   kubernetes.io/service-account-token   3         20d
gitlab-token-jcjjx    kubernetes.io/service-account-token   3         20d

$ kubectl get secret gitlab-token-jcjjx -o jsonpath="{['data']['token']}" | base64 -D

$ kubectl get secret gitlab-token-jcjjx -o jsonpath="{['data']['ca\.crt']}" | base64 -D
```

### Préparation du cluster

Créer un ClusterRoleBinding cluster-admin
```yaml
$ kubectl create -f - <<EOF
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: gitlab-cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab
  namespace: default
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
EOF
```

### Dans GitLab

Enregistrer le cluster dans son projet

* API URL
* CA
* Token

```shell-session
$ kubectl get namespace
NAME                  STATUS    AGE
default               Active    5m
gitlab-k8s-9417974    Active    2m
kube-public           Active    5m
kube-system           Active    5m
```

### L'accès au cluster

Depuis la version 11.5, GitLab CI ne va pas utiliser le token servant à l'enregistrement pour accéder au cluster, il va créer un compte de service dans le namespace du projet

```shell-session
$ kubectl describe rolebindings.rbac.authorization.k8s.io -n gitlab-k8s-9417974 gitlab-gitlab-k8s-9417974
Name:         gitlab-gitlab-k8s-9417974
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  ClusterRole
  Name:  edit
Subjects:
  Kind            Name                                Namespace
  ----            ----                                ---------
  ServiceAccount  gitlab-k8s-9417974-service-account  gitlab-k8s-9417974
```

### Pour aller plus loin

GitLab propose d'ajouter quelques applications au cluster, toutes les applications sont déployées via des charts Helm dans le namespace *gitlab-managed-apps*

* Helm Tiller
* GitLab Runner
* Prometheus
* NGINX Ingress Controller
* CertManager
* JupyterHub (notebook)
* Knative (serverless)

### Pour aller plus loin

```shell-session
$ kubectl get pods -n gitlab-managed-apps
NAME                                             READY     STATUS    RESTARTS   AGE
ingress-nginx-ingress-controller-ff666c548-tp2pl         1/1       Running   0          2m
ingress-nginx-ingress-default-backend-6679dd498c-6b56v   1/1       Running   0          2m
prometheus-kube-state-metrics-6584885ccf-hch7k   1/1       Running   0          2m
prometheus-prometheus-server-69b9f444df-s8c2q    2/2       Running   0          2m
Runner-gitlab-Runner-db5669b89-77dv5             1/1       Running   0          11m
tiller-deploy-789dcbff8b-vgwbn                   1/1       Running   0          12m
```

### Et maintenant, le CI

Plusieurs variables sont passées à chaque exécution du CI

* $KUBE_URL
* $KUBE_TOKEN
* $KUBE_NAMESPACE
* $KUBE_CA_PEM_FILE
* $KUBECONFIG

Par défaut le CLI *kubectl* utilise la variable $KUBECONFIG pour charger sa configuration

### Et maintenant, le CI

```yaml
deploy_staging:
  stage: deploy
  image: debian:stable-slim
  script:
    - apt-get update
    - apt-get install --no-install-recommends  -y openssl curl bash ca-certificates
    - curl -L -s -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    - chmod +x /usr/bin/kubectl
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" k8s/*.yaml
    - sed -i "s/__REPLICAS__/1/" k8s/*.yaml
    - sed -i "s/__TAG__/latest/" k8s/*.yaml
    - kubectl apply -f k8s/
  environment:
    name: staging
    url: http://slides-${CI_ENVIRONMENT_SLUG}.myk8S.ga
  only:
    - master
```

### Le monitoring

Pour remonter les métriques de nos différents environnements dans GitLab il est important de respecter une certaine norme de nommage

Le nom des objets Deployment ou DaemonSet doit commencer par la variable *${CI_ENVIRONMENT_SLUG}*

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: __CI_ENVIRONMENT_SLUG__
  labels:
    app: __CI_ENVIRONMENT_SLUG__
```

### Les limites

En dehors de sa version Premium, GitLab ne permet d'intégrer qu'un seul cluster (voir un seul namespace) à un projet, tous les déploiements seront donc dans un espace commun

On peut cependant utiliser les variables pour configurer le CLI et accéder à un autre namespace ou cluster

Cependant, l'intégration sera perdue, plus de monitoring (ou presque), plus de logs, plus de console...

### Les limites 
```yaml
  - echo "${K8S_PROD_CA}" > /tmp/ca.cert
  - kubectl config set-cluster k8s_prod --server="${K8S_PROD_URL}" --certificate-authority=/tmp/ca.cert
  - kubectl config set-credentials gitlab --token="${K8S_PROD_TOKEN}"
  - kubectl config set-context k8s_prod --cluster=k8s_prod --user=gitlab --namespace=rocketchat
  - kubectl config use-context k8s_prod
```
##  La démo ! {data-background="img/demo.gif"}

## Merci