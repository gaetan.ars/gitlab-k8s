variables:
  KUBERNETES_VERSION: 1.12.1
  DOCKER_HOST: tcp://localhost:2375
  CONTAINER_IMAGE_BUILT: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}
  CONTAINER_IMAGE_RELEASE: ${CI_REGISTRY_IMAGE}:latest
  CONTAINER_IMAGE_TAG: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}

stages:
  - build
  - test
  - release
  - scan
  - tag
  - deploy

build_pages:
  stage: build
  image: zenman94/pandoc:2.4
  script:
    - pandoc -s -f markdown -t revealjs -o index.html slides.md -V revealjs-url=https://revealjs.com -V transition=concave -V theme=blood --include-in-header=css/perso.css
  artifacts:
    paths:
    - index.html
    expire_in: 1 hour
  only:
    - pages

build_image:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build -t ${CONTAINER_IMAGE_BUILT} .
    - docker push ${CONTAINER_IMAGE_BUILT}
  except:
    - pages
    - tags

test_image:
  stage: test
  script:
    - exit 0
  except:
    - pages
    - tags

release:
  stage: release
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull ${CONTAINER_IMAGE_BUILT}
    - docker tag ${CONTAINER_IMAGE_BUILT} ${CONTAINER_IMAGE_RELEASE}
    - docker push ${CONTAINER_IMAGE_RELEASE}
  only:
    - master

scan_image:
  stage: scan
  image: docker:stable
  allow_failure: true
  services:
    - docker:stable-dind
    - postgres:latest
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker run -d --name db arminc/clair-db:latest
    - docker run -p 6060:6060 --link db:postgres -d --name clair --restart on-failure arminc/clair-local-scan:v2.0.6
    - apk add -U wget ca-certificates
    - echo "docker pull ${CONTAINER_IMAGE_RELEASE}"
    - docker pull ${CONTAINER_IMAGE_RELEASE}
    - echo "wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64"
    - wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64
    - mv clair-scanner_linux_amd64 clair-scanner
    - chmod +x clair-scanner
    - touch clair-whitelist.yml
    - retries=0
    - echo "Waiting for clair daemon to start"
    - docker ps
    - while( ! wget -T 10 http://127.0.0.1:6060/v1/namespaces ) ; do sleep 1 ; echo -n "." ; if [ $retries -eq 10 ] ; then echo " Timeout, aborting." ; exit 1 ; fi ; retries=$(($retries+1)) ; done
    - ./clair-scanner -c http://127.0.0.1:6060 --ip $(hostname -i) -r gl-container-scanning-report.json -l clair.log -w clair-whitelist.yml ${CONTAINER_IMAGE_RELEASE} || true
  artifacts:
    paths: [gl-container-scanning-report.json]
  only:
    - master
  except:
    variables:
      - $CONTAINER_SCANNING_DISABLED

tag_image:
  stage: tag
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker pull ${CONTAINER_IMAGE_RELEASE}
    - docker tag  ${CONTAINER_IMAGE_RELEASE} ${CONTAINER_IMAGE_TAG}
    - docker push ${CONTAINER_IMAGE_TAG}
  only:
    - tags

deploy_review:
  stage: deploy
  image: debian:stable-slim
  script:
    - apt-get update
    - apt-get install --no-install-recommends  -y openssl curl bash ca-certificates
    - curl -L -s -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    - chmod +x /usr/bin/kubectl
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" k8s/*.yaml
    - sed -i "s/__REPLICAS__/1/" k8s/*.yaml
    - sed -i "s/__TAG__/${CI_COMMIT_REF_SLUG}_${CI_COMMIT_SHA}/" k8s/*.yaml
    - kubectl apply -f k8s/
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://slides-${CI_ENVIRONMENT_SLUG}.myk8S.ga
    on_stop: stop_review
  only:
    - branches
  except:
    - master

stop_review:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  script:
    - echo "Remove review app"
    - apt-get update
    - apt-get install --no-install-recommends  -y openssl curl bash ca-certificates
    - curl -L -s -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    - chmod +x /usr/bin/kubectl
    - kubectl delete ing -l app=${CI_ENVIRONMENT_SLUG}
    - kubectl delete all -l app=${CI_ENVIRONMENT_SLUG}
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  only:
    - branches
  except:
    - master
    - pages

deploy_staging:
  stage: deploy
  image: debian:stable-slim
  script:
    - apt-get update
    - apt-get install --no-install-recommends  -y openssl curl bash ca-certificates
    - curl -L -s -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    - chmod +x /usr/bin/kubectl
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" k8s/*.yaml
    - sed -i "s/__REPLICAS__/1/" k8s/*.yaml
    - sed -i "s/__TAG__/latest/" k8s/*.yaml
    - kubectl apply -f k8s/
  environment:
    name: staging
    url: http://slides-${CI_ENVIRONMENT_SLUG}.myk8S.ga
  only:
    - master

deploy_prod:
  stage: deploy
  image: debian:stable-slim
  script:
    - apt-get update
    - apt-get install --no-install-recommends  -y openssl curl bash ca-certificates
    - curl -L -o /usr/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl"
    - chmod +x /usr/bin/kubectl
    - sed -i "s/__CI_ENVIRONMENT_SLUG__/${CI_ENVIRONMENT_SLUG}/" k8s/*.yaml
    - sed -i "s/__REPLICAS__/3/" k8s/*.yaml
    - sed -i "s/__TAG__/${CI_COMMIT_TAG}/" k8s/*.yaml
    - kubectl apply -f k8s/
  environment:
    name: production
    url: http://slides-${CI_ENVIRONMENT_SLUG}.myk8S.ga
  only:
    - tags

pages:
  stage: deploy
  script:
    - mkdir .public
    - cp index.html .public
    - cp -r img .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - pages